<?php

namespace Drupal\www_authenticate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure http access for this site.
 */
class WwwAuthenticateForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['www_authenticate.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'www_authenticate_config_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('www_authenticate.config');

    $form['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('WWW-Authenticate Credentials'),
      '#description' => $this->t('After receiving the WWW-Authenticate header, a client will typically prompt the user for credentials, and then re-request the resource.'),
    ];
    $form['credentials']['login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Authentication username'),
      '#default_value' => $config->get('login'),
    ];
    $form['credentials']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Authentication password'),
      '#default_value' => $config->get('password'),
    ];
    $form['credentials']['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Text to send if user hits Cancel button'),
      '#default_value' => $config->get('message'),
    ];
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#description' => $this->t('Enable the WWW-Authenticate'),
      '#default_value' => $config->get('enabled'),
    ];
    $form['flush_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Flush all caches'),
      '#description' => $this->t('Flush all caches to implement the configuration changes or manually flush the caches'),
      '#default_value' => $config->get('flush_cache'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $enabled = $form_state->getValue('enabled');
    $login = trim($form_state->getValue('login'));
    $password = trim($form_state->getValue('password'));
    $flush_cache = trim($form_state->getValue('flush_cache'));
    $message = trim($form_state->getValue('message'));

    $config = $this->config('www_authenticate.config');
    if ($login == '' || $password == '') {
      $enabled = FALSE;
      $flush_cache = FALSE;
    }
    $config->set('enabled', $enabled);
    $config->set('login', $login);
    $config->set('password', $password);
    $config->set('message', $message);
    $config->set('flush_cache', $flush_cache);
    $config->save();

    if ($flush_cache) {
      drupal_flush_all_caches();
    }

    return parent::submitForm($form, $form_state);

  }

}
