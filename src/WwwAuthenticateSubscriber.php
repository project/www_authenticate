<?php

namespace Drupal\www_authenticate;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Provides a WwwAuthenticateSubscriber.
 */
class WwwAuthenticateSubscriber implements EventSubscriberInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new MyCustomService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Only if KernelEvents::REQUEST !!!
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   *
   * @see Symfony\Component\HttpKernel\KernelEvents
   */
  public function wwwauthenticateload(RequestEvent $event) {

    // Get config.
    $config = $this->configFactory->get('www_authenticate.config');

    // Do nothing if disabled.
    if (!$config->get('enabled')) {
      return;
    }

    // Retrieve the user and pass from config.
    $user = $config->get('login');
    $pass = $config->get('password');

    // Test basic auth credentials.
    if (!empty($_SERVER['PHP_AUTH_USER'])
      && isset($_SERVER['PHP_AUTH_PW'])
      && $_SERVER['PHP_AUTH_USER'] === $user
      && $_SERVER['PHP_AUTH_PW'] === $pass
    ) {
      // Authentication passes, do nothing.
      return;
    }
    else {

      // Authentication failed, print the authorization headers
      // and end the request.
      $print = $config->get('message');
      $sitename = $this->configFactory->get('system.site')->get('name');

      $config = $this->configFactory->get('www_authenticate.config');

      header(sprintf('WWW-Authenticate: Basic realm="%s"', $sitename));
      header('HTTP/1.0 401 Unauthorized');

      die($print);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Add wwwauthenticateload as a subscriber to the request event.
    // $events[KernelEvents::REQUEST][] = array('wwwauthenticateload', 20);.
    $events[KernelEvents::REQUEST][] = ['wwwauthenticateload'];
    return $events;
  }

}
